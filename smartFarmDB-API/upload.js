const IncomingForm = require('formidable').IncomingForm
const fs = require('fs');

module.exports = function upload(req, res) {
    var form = new IncomingForm({ 
        uploadDir: "./UploadedFiles/"
    })
    form.on('file', (field, file) => {})

    form.on('end', () => {
        res.json()
    })
    form.parse(req, (err, fields, files) => {
        const fileName = files.file.name
        const newFilePath = "./UploadedFiles/" + fileName
        console.log("Moving " + fileName + " to " + newFilePath)
        fs.rename(files.file.path, newFilePath, (err) => {
            if(err){
                console.error(err)
                console.error("Error: Unable to move file: " + files.file.path)
            }
        })
      });
}