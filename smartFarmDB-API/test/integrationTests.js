let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../index.js");

//Assertion Style
chai.should();

chai.use(chaiHttp);

//Rename cow to local cow_id before running tests
const cow = 's18'
//Make sure cow 'M420' doesn't exist before running POST, PUT, PATCH, DELETE tests
const cowPut = 'M420'
const cowPostData = {
    "cow_id": "M420",
    "date_of_birth": "2020-01-01"
}
const cowPutData = {
    "date_of_birth": "2020-02-02",
    "date_of_entry": "2020-03-01",
    "date_of_exit": ""
}
const collar = '1234'
const collPostData = { 
    "collar_id": "1234",
    "collar_ser_no": "4324223235", 
    "collar_num": "s19"
 }
 const collPutData = { 
    "collar_ser_no": "555555555",
    "collar_num": "s25", 
    "date_of_entry": "2020-03-05"
}

describe('smartFarmDB API', () => {

    describe('GET /actday/:cowId/:date', () => {
        it("It should get 24 hourly activity counts for a given cow and day, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/actday/"+cow+"/2020-03-03")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /actday/:cowId/:date', () => {
        it("It should return parameters day, hour, hourSummaries & correct date", (done) => {
            chai.request(server)
                .get("/actday/"+cow+"/2020-03-03")
                .end((error, response) => {
                    (response.text).should.contain("day");
                    (response.text).should.contain("hour");
                    (response.text).should.contain("hourSummaries");
                    (response.text).should.contain("2020-03-03");                  
                    done()
                })
        })
    })

    describe('GET /locday/:cowId/:date', () => {
        it("It should get location readings for a given cow and day, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/locday/"+cow+"/2020-03-03")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /locday/:cowId/:date', () => {
        it("It should return parameters loc_date_time, loc_lat, loc_lon & correct date", (done) => {
            chai.request(server)
                .get("/locday/"+cow+"/2020-03-03")
                .end((error, response) => {
                    (response.text).should.contain("loc_date_time");
                    (response.text).should.contain("loc_lat");
                    (response.text).should.contain("loc_lon");
                    (response.text).should.contain("2020-03-03");                  
                    done()
                })
        })
    })

    describe('GET /cowlist', () => {
        it("It should get all of the cows and their collars, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/cowlist")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /cowlist', () => {
        it("It should return parameters cow_id and collar_id", (done) => {
            chai.request(server)
                .get("/cowlist")
                .end((error, response) => {                   
                    (response.text).should.contain("cow_id");
                    (response.text).should.contain("collar_id");                    
                    done()
                })
        })
    })

    describe('GET /battdate/:cowId/:date', () => {
        it("It should get battery measurements for a given cow and day, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/battdate/"+cow+"/2020-03-04")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /battdate/:cowId/:date', () => {
        it("It should return parameters for battery readings and correct date", (done) => {
            chai.request(server)
                .get("/battdate/"+cow+"/2020-03-04")
                .end((error, response) => {
                    (response.text).should.contain("BATT_DATE");
                    (response.text).should.contain("BATT_MAX");
                    (response.text).should.contain("BATT_MIN");
                    (response.text).should.contain("BATT_00_00");
                    (response.text).should.contain("BATT_04_00");
                    (response.text).should.contain("BATT_08_00");
                    (response.text).should.contain("BATT_12_00");
                    (response.text).should.contain("BATT_16_00");
                    (response.text).should.contain("BATT_20_00");
                    (response.text).should.contain("2020-03-04");                  
                    done()
                })
        })
    })

    describe('GET /tempdate/:cowId/:date', () => {
        it("It should get temperature measurements for a given cow and day, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/tempdate/"+cow+"/2020-03-04")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /tempdate/:cowId/:date', () => {
        it("It should return parameters for temperature readings and correct date", (done) => {
            chai.request(server)
                .get("/tempdate/"+cow+"/2020-03-04")
                .end((error, response) => {
                    (response.text).should.contain("TEMP_DATE");
                    (response.text).should.contain("TEMP_MAX");
                    (response.text).should.contain("TEMP_00_00");
                    (response.text).should.contain("TEMP_04_00");
                    (response.text).should.contain("TEMP_08_00");
                    (response.text).should.contain("TEMP_12_00");
                    (response.text).should.contain("TEMP_16_00");
                    (response.text).should.contain("TEMP_20_00");
                    (response.text).should.contain("2020-03-04");                  
                    done()
                })
        })
    })
    
    describe('GET /battrange/:cowId/:startDate/:endDate', () => {
        it("It should get min, max & avg battery measurements for each day for a given cow and date range, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/battrange/"+cow+"/2020-03-04/2020-03-10")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /battrange/:cowId/:startDate/:endDate', () => {
        it("It should return parameters for max, min & average battery reads", (done) => {
            chai.request(server)
                .get("/battrange/"+cow+"/2020-03-04/2020-03-10")
                .end((error, response) => {
                    (response.text).should.contain("MAX");
                    (response.text).should.contain("MIN");
                    (response.text).should.contain("AVG");                  
                    done()
                })
        })
    })

    describe('GET /temprange/:cowId/:startDate/:endDate', () => {
        it("It should get min, max & avg temperature measurements for each day for a given cow and date range, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/temprange/"+cow+"/2020-03-04/2020-03-10")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /temprange/:cowId/:startDate/:endDate', () => {
        it("It should return parameters for max, min & average temperature reads", (done) => {
            chai.request(server)
                .get("/temprange/"+cow+"/2020-03-04/2020-03-10")
                .end((error, response) => {
                    (response.text).should.contain("MAX");
                    (response.text).should.contain("MIN");
                    (response.text).should.contain("AVG");                  
                    done()
                })
        })
    })

    describe('GET /weight/:cowId', () => {
        it("It should get all of the cows and their collars, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/weight/"+cow)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    //weight data requires adding to database first to return proper fields
    describe('GET /weight/:cowId', () => {
        it("It should return parameters cow_id and collar_id", (done) => {
            chai.request(server)
                .get("/weight/"+cow)
                .end((error, response) => {                   
                    (response.text).should.contain("cow_id");
                    (response.text).should.contain("collar_id");
                    (response.text).should.contain("weighed_by");
                    (response.text).should.contain("weigh_date");
                    (response.text).should.contain("weight_kg");                    
                    done()
                })
        })
    })

    
    describe('GET /cow', () => {
        it("It should get all of the cows and their dates, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/cow")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /cow', () => {
        it("It should return parameters cow_id, date_of_birth, date_of_entry & date_of_exit", (done) => {
            chai.request(server)
                .get("/cow")
                .end((error, response) => {                   
                    (response.text).should.contain("COW_ID");
                    (response.text).should.contain("DATE_OF_BIRTH");
                    (response.text).should.contain("DATE_OF_ENTRY");
                    (response.text).should.contain("DATE_OF_EXIT");                    
                    done()
                })
        })
    })

    describe('GET /cow/:cowId', () => {
        it("It should get all of the cows and their dates, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/cow/"+cow)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /cow/:cowId', () => {
        it("It should return parameters cow_id, date_of_birth, date_of_entry & date_of_exit", (done) => {
            chai.request(server)
                .get("/cow/"+cow)
                .end((error, response) => {                   
                    (response.text).should.contain("s18");
                    (response.text).should.contain("COW_ID");
                    (response.text).should.contain("DATE_OF_BIRTH");
                    (response.text).should.contain("DATE_OF_ENTRY");
                    (response.text).should.contain("DATE_OF_EXIT");                    
                    done()
                })
        })
    })

    describe('POST /cow/', () => {
        it("It should return 201 to indicate cow has been added to database", (done) => {
            chai.request(server)
                .post("/cow/")
                .send(cowPostData)
                .end((error, response) => {                   
                    response.should.have.status(201);                   
                    done()
                })
        })
    })

    describe('PUT /cow/:cowId', () => {
        it("It should return 200 status and contain text indicating changed row", (done) => {
            chai.request(server)
                .put("/cow/"+cowPut)
                .send(cowPutData)
                .end((error, response) => {                   
                    response.should.have.status(200);  
                    (response.text).should.contain('Rows matched: 1');
                    (response.text).should.contain('Changed: 1');                
                    done()
                })
        })
    })

    describe('PATCH /cow/:cowId', () => {
        it("It should return 200 status and contain text indicating retired cow", (done) => {
            chai.request(server)
                .patch("/cow/"+cowPut)
                .end((error, response) => {                   
                    response.should.have.status(200);  
                    (response.text).should.contain('Rows matched: 1');
                    (response.text).should.contain('Changed: 1');                   
                    done()
                })
        })
    })

    describe('DELETE /cow/:cowId', () => {
        it("It should return 200 status and contain text indicating deleted cow", (done) => {
            chai.request(server)
                .delete("/cow/"+cowPut)
                .end((error, response) => {                   
                    response.should.have.status(200);
                    (response.text).should.contain('"affectedRows":1');                  
                    done()
                })
        })
    })


    describe('POST /collar/', () => {
        it("It should return 201 to indicate collar has been added to database", (done) => {
            chai.request(server)
                .post("/collar/")
                .send(collPostData)
                .end((error, response) => {                   
                    response.should.have.status(201);                   
                    done()
                })
        })
    })

    describe('PUT /collar/:cowId', () => {
        it("It should return 200 status and contain text indicating changed row", (done) => {
            chai.request(server)
                .put("/collar/"+collar)
                .send(collPutData)
                .end((error, response) => {                   
                    response.should.have.status(200);  
                    (response.text).should.contain('Rows matched: 1');
                    (response.text).should.contain('Changed: 1');                
                    done()
                })
        })
    })

    describe('GET /collar', () => {
        it("It should get all of the cows and their dates, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/cow")
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /collar', () => {
        it("It should return parameters cow_id, date_of_birth, date_of_entry & date_of_exit", (done) => {
            chai.request(server)
                .get("/cow")
                .end((error, response) => {                   
                    (response.text).should.contain('"COLLAR_ID": "1234"');
                    (response.text).should.contain('"COLLAR_SER_NO": "4324223235"');
                    (response.text).should.contain('"COLLAR_NUM": "s19"');
                    (response.text).should.contain('"DATE_OF_ENTRY":');                  
                    done()
                })
        })
    })

    describe('GET /collar/:collarId', () => {
        it("It should get all of the cows and their dates, returning 200 OK, error status null", (done) => {
            chai.request(server)
                .get("/cow/"+cow)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');                   
                    done()
                })
        })
    })

    describe('GET /collar/:collarId', () => {
        it("It should return parameters cow_id, date_of_birth, date_of_entry & date_of_exit", (done) => {
            chai.request(server)
                .get("/cow/"+cow)
                .end((error, response) => {                   
                    (response.text).should.contain('"COLLAR_ID": "1234"');
                    (response.text).should.contain('"COLLAR_SER_NO": "4324223235"');
                    (response.text).should.contain('"COLLAR_NUM": "s19"');
                    (response.text).should.contain('"DATE_OF_ENTRY":');                  
                    done()
                })
        })
    })

});

