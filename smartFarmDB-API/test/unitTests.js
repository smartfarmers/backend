let chai = require("chai");

chai.should();

//Constant values to pass through functions for unit testing
const tempArray = [{
    TEMP_DATE: '2020-03-07',
    MAX: 44,
    '00_00': 33,
    '04_00': 26,
    '08_00': 27,
    '12_00': 25,
    '16_00': 33,
    '20_00': 35
  }];

const battArray = [{
      BATT_DATE: '2020-03-07',
      BATT_MIN: 31,
      BATT_MAX: 41,
      BATT_00_00: 4000,
      BATT_04_00: 3900,
      BATT_08_00: 3600,
      BATT_12_00: 3200,
      BATT_16_00: 3900,
      BATT_20_00: 4000
    }];

const testArray = ['33', '26', '27', '25', '33', '36']

//Unit Tests for four main Functions
describe('smartFarmDB API Functions Unit Tests', () => {

    describe('batteryArray', () => {
        it("Should return correct array format with date and object values for min, max & avg", (done) => {
            array = createBattRangeArray(battArray);
            arr = array[0];
            reads = arr[1];
            arr.should.be.a('array');
            arr.should.have.length(2);
            arr.should.contain("2020-03-07");
            reads[0]['MIN'].should.equal('3100')
            reads[1]['MAX'].should.equal('4100')
            reads[2]['AVG'].should.equal('3767')            
            done();
        })
    })
    
    describe('temperatureArray', () => {
        it("Should return correct array format with date and object values for min, max & avg", (done) => {
            array = createTempRangeArray(tempArray);
            arr = array[0];
            reads = arr[1];
            arr.should.be.a('array');
            arr.should.have.length(2);
            arr.should.contain("2020-03-07");
            reads[0]['MAX'].should.equal('44')
            reads[1]['MIN'].should.equal('25')
            reads[2]['AVG'].should.equal('29.83')            
            done();
        })
    })

    describe('getAverages', () => {
        it("Should return correct value of average from test array of numbers", (done) => {
            var avg = getAverages(testArray);
            avg.should.be.a('number');            
            avg.should.equal(30);        
            done();
        })
    })

    describe('getMinimum', () => {
        it("Should return correct value of minimum from test array of numbers", (done) => {
            var avg = getMinimum(testArray);
            avg.should.be.a('number');            
            avg.should.equal(25);        
            done();
        })
    })

//Functions being tested
    function createBattRangeArray(results) {
        var endArray = [];
        for (var i = 0; i < results.length; i++) {     //Loop through each day
            var xArray = {}
            var row = results[i];
            var str = JSON.stringify(row);
            str = str.substring(1, (str.length - 1))  
            var arr = str.split(",");
            var date = arr[0].substring(13, (arr[0].length - 1));
            var readings = [];
            var batts = [];
            for (var j = 1; j < 9; j++) {   //Loop through each reading, push each temp number into an array to push into xArray
                var r = arr[j].split(":");
                batts.push(r[1]);
            }
            var max = batts.shift();
            var min = batts.shift();
            var avg = Math.round(getAverages(batts));
            readings = [{"MIN": max+"00"}, {"MAX": min+"00"}, {"AVG": avg.toString()}];
            xArray = [date, readings];  //xArray contains date and temperature array
            endArray.push(xArray);  //xArray appended to overall array to be returned 
        }
            return endArray
    }
    
    function createTempRangeArray(results) {
        var endArray = [];
        for (var i = 0; i < results.length; i++) {     //Loop through each day
            var xArray = {}
            var row = results[i];
            var str = JSON.stringify(row);
            str = str.substring(1, (str.length - 1))  
            var arr = str.split(",");
            var date = arr[0].substring(13, (arr[0].length - 1));
            var readings = [];
            var temps = [];
            for (var j = 1; j < 8; j++) {   //Loop through each reading, push each temp number into an array to push into xArray
                var r = arr[j].split(":");
                temps.push(r[1]);
            }
            var max = temps.shift();
            var avg = getAverages(temps).toFixed(2);
            var min = getMinimum(temps);
            readings = [{"MAX": max}, {"MIN": min.toString()}, {"AVG": avg.toString()}];
            xArray = [date, readings];  //xArray contains date and temperature array
            endArray.push(xArray);  //xArray appended to overall array to be returned 
        }
            return endArray
    }
    
    function getMinimum(reads) {
        var arr = [];
        for (var i = 0; i < reads.length; i++) {
            var num = reads[i];
            if (num != "0") {
                arr.push(parseInt(num));
            }
        }
        var min = arr[0];
        for (var i = 1; i < arr.length; i++){
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return min
    }
    
    function getAverages(reads) {
        var sum = [];
        for (var i = 0; i < reads.length; i++) {
            var num = reads[i];
            if (num != "0"){
                sum.push(parseInt(num));
            }
        }
        var count = 0;
        for(var i = 0; i < sum.length; i++) {
            count += parseInt(sum[i]); 
        }
        var avg = count/sum.length;
        return avg;
        }

});