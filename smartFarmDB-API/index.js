const express = require('express');
const bodyParser = require('body-parser');
const app  = express();
const mysql = require('mysql');
const Promise = require('promise');
const cors = require('cors');
const upload = require('./upload');
const socketIO = require('socket.io'); 
const http = require('http');
const { spawn } = require('child_process');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const passport = require('passport');
const passportJWT = require('passport-jwt');
const { Console } = require('console');
const { table } = require('console');
const fastcsv = require("fast-csv");
const fs = require("fs");
const e = require('express');
const { ValidationError } = require('sequelize');

app.use(bodyParser.json());
app.use(passport.initialize());

const saltRounds = 10;
const port = 3000;

//Authentication Processes

const sequelize = new Sequelize({
    database: 'smartFarm',
    username: 'root',
    password: 'P@ssword1',
    dialect: 'mysql',
  });

//Creates & Verifies User table for authentication 
const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        unique: true
      },
    password: {
        type: Sequelize.STRING,
      },
    }); 
    
User.sync()
    .then(() => console.log('User table configured successfully'))
    .catch(err => console.log('Database Error'));


      
const getUser = async obj => {
    return await User.findOne({
    where: obj,
    });
};

//Setup Passport Authentication
let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;
let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'secret';

let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log('payload received', jwt_payload);
    let user = getUser({ id: jwt_payload.id });
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  });
  
passport.use(strategy);

// Report Constants
const tempDailyFileName = "./TempGeneratedReports/tempDaily.csv"
const tempAct4FileName = "./TempGeneratedReports/tempActivityFourSecond.csv"
const tempLocationFiveFileName = "./TempGeneratedReports/tempLocationFive.csv"
const tempTempFileName = "./TempGeneratedReports/tempTemp.csv"
const tempWeightFileName = "./TempGeneratedReports/tempWeight.csv"

// Setup cors
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions));


//Handle file upload
app.post('/upload', upload)

//Database connection configuration
//const dbConn = mysql.createConnection({
//    host: 'localhost',
//    user: 'root',
//    password: 'P@ssword1',
//    database: 'smartFarm',
//    dateStrings: [
//        'DATE',
//        'DATETIME'
//    ]
//});

//dbConn.connect((error) =>{
//    if(error) throw error;
//    console.log('Database connection secured')
//})


const dbConn_config ={
    host: 'localhost',
    user: 'root',
    password: 'P@ssword1',
    database: 'smartFarm',
    dateStrings: [
        'DATE',
        'DATETIME'
    ]
};

var dbConn;

function handleDisconnect() {
  dbConn = mysql.createConnection(dbConn_config); // Recreate the connection, since
                                                  // the old one cannot be reused.

  dbConn.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  dbConn.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

let server = http.createServer(app)
let io = socketIO(server)

// make a connection with the user from server side
io.on('connection', (socket)=>{
    socket.on("fileImport", fileName => {
        console.log("Importing File: " + fileName)
        const importFilePath = __dirname + "/UploadedFiles/" + fileName
        // spawn new child process to call the python script
        console.log(importFilePath)
        const python = spawn('python3', ["../dataimport/CleanImportDataIntoDB.py", importFilePath]);
        python.stdout.on('data', function(data) {
            let output = String(data);
            if(output.includes(`${fileName} Import Complete`)){
                console.log(output)
                console.log("Send " + fileName + " is complete");
                socket.emit("fileImport", fileName + " uploaded");
            } else if(output.includes("Progress:")){
                console.log(output)
                let progress = output.split(" ")[1]
                progress = progress.split("/")
                progress = parseFloat(progress[0]) / parseFloat(progress[1])
                console.log("progress Percent: " + progress)
                socket.emit("fileImport", fileName + " progress " + progress);
            } else if(
                output.includes("Unable to find a collar ID") ||
                output.includes("No cow has currently been allocated") ||
                output.includes("ERROR: Unable to determine the file type") ||
                output.includes("already imported")
                ){
                console.log(output)
                socket.emit("fileImport", output)
            } else {
                console.log(output)
            }
        });
    })
  });


function getTableName(collarId, tableType) {
    if (tableType === "act"){
        return "ACTIVITY_FOUR_SECS_" + collarId
    } else if (tableType ==="loc"){
        return "LOCATION_FIVE_MINS_" + collarId
    }
}

async function getAveragesSql(cowId, tableType) {
    var collarId = await getCollarId(cowId)
    var tableName = getTableName(collarId, tableType)
    if (tableType == "act"){
        return activityAveragesForHour = "SELECT "+tableName+".act_stat, count(*) as count " +
        "FROM "+tableName+" INNER JOIN COLLAR_ALLOCATION ON "+tableName+".collar_loc_id = COLLAR_ALLOCATION.collar_loc_id " +
        "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
        "WHERE COW.cow_id = ? && DATE(act_date_time) = ? && (TIME(act_date_time) >= ? && TIME(act_date_time) < ?) " + 
        "GROUP BY "+tableName+".act_stat ORDER BY count DESC"
    } else if (tableType === "loc") {
        return locationAveragesForHour = "SELECT loc_date_time, loc_lat, loc_lon FROM "+tableName+" " +
        "INNER JOIN COLLAR_ALLOCATION ON "+tableName+".collar_loc_id = COLLAR_ALLOCATION.collar_loc_id " +
        "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
        "WHERE COW.cow_id = ? && DATE(loc_date_time) = ?"
    }
}

function getCollarId(cowId){
    return new Promise(async function(resolve, reject) {
        try {
            let sql = 'SELECT COLLAR_ID FROM COLLAR_ALLOCATION WHERE COW_ID = ?;'
            let results = await dbConn.query(sql, cowId, function (error, results, field) {
                if (error) {
                    console.log(error);
                    reject(error)
                }
                else {
                    resolve(results[0].COLLAR_ID);
                }
            });
        } catch {
            console.error("Server error");
            reject()
        }
    })
}


//Functions
function createBattRangeArray(results) {
    var endArray = [];
    for (var i = 0; i < results.length; i++) {     //Loop through each day
        var xArray = {}
        var row = results[i];
        var str = JSON.stringify(row);
        str = str.substring(1, (str.length - 1))
        var arr = str.split(",");
        var date = arr[0].substring(13, (arr[0].length - 1));
        var readings = [];
        var batts = [];
        for (var j = 1; j < 9; j++) {   //Loop through each reading, push each temp number into an array to push into xArray
            var r = arr[j].split(":");
            batts.push(r[1]);
        }
        var max = batts.shift();
        var min = batts.shift();
        var avg = Math.round(getAverages(batts));
        readings = [{"MIN": max+"00"}, {"MAX": min+"00"}, {"AVG": avg.toString()}];
        xArray = [date, readings];  //xArray contains date and temperature array
        endArray.push(xArray);  //xArray appended to overall array to be returned 
    }
        return endArray
}

function createTempRangeArray(results) {
    var endArray = [];
    for (var i = 0; i < results.length; i++) {     //Loop through each day
        var xArray = {}
        var row = results[i];
        var str = JSON.stringify(row);
        str = str.substring(1, (str.length - 1)) 
        var arr = str.split(",");
        var date = arr[0].substring(13, (arr[0].length - 1));
        var readings = [];
        var temps = [];
        for (var j = 1; j < 8; j++) {   //Loop through each reading, push each temp number into an array to push into xArray
            var r = arr[j].split(":");
            temps.push(r[1]);
        }
        var max = temps.shift();
        var avg = getAverages(temps).toFixed(2);
        var min = getMinimum(temps);
        readings = [{"MAX": max}, {"MIN": min.toString()}, {"AVG": avg.toString()}];
        xArray = [date, readings];  //xArray contains date and temperature array
        endArray.push(xArray);  //xArray appended to overall array to be returned 
    }
        return endArray
}

function getMinimum(reads) {
    var arr = [];
    for (var i = 0; i < reads.length; i++) {
        var num = reads[i];
        if (num != "0") {
            arr.push(parseInt(num));
        }
    }
    var min = arr[0];
    for (var i = 1; i < arr.length; i++){
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min
}

function getAverages(reads) {
    var sum = [];
    for (var i = 0; i < reads.length; i++) {
        var num = reads[i];
        if (num != "0"){
            sum.push(parseInt(num));
        }
    }
    var count = 0;
    for(var i = 0; i < sum.length; i++) {
        count += parseInt(sum[i]); 
    }
    var avg = count/sum.length;
    return avg;
    }

function getCurrentDate() {
    let date = new Date();
    let day = ("0" + date.getDate()).slice(-2);
    let month = ("0" + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let today = (year + "-" + month + "-" + day)
    return today
}

function checkValidDate(date) {
    if (date == "") {
        let date = undefined
        return date
    } 
    return date
}

//SQL Queries
const returnCowList = "SELECT COW.cow_id, COLLAR.collar_id FROM COW " +
    "INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
    "INNER JOIN COLLAR ON COLLAR.collar_id = COLLAR_ALLOCATION.collar_id WHERE COLLAR_ALLOCATION.date_off IS NULL "

const returnCows = "SELECT DISTINCT COW.*, COLLAR.collar_num, COLLAR.collar_id, COLLAR_ALLOCATION.date_off FROM COW " +
    "LEFT JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
    "LEFT JOIN COLLAR ON COLLAR_ALLOCATION.collar_id = COLLAR.collar_id"

const returnAllCowID = "SELECT cow_id FROM COW";

const returnCollars = "SELECT DISTINCT COLLAR.*, COLLAR_ALLOCATION.collar_loc_id FROM COLLAR " + 
    "LEFT JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_id = COLLAR.collar_id"

const cowPut = "UPDATE COW SET cow_id = ?, date_of_birth = ?, date_of_entry = ?, date_of_exit = ? WHERE cow_id = ?";

const allocateCollar = "INSERT INTO COLLAR_ALLOCATION (cow_id, collar_id, date_on) VALUES (?, ?, ?)";

const removeCollar = "UPDATE COLLAR_ALLOCATION SET date_off = ? WHERE collar_id = ? ORDER BY collar_loc_id DESC LIMIT 1";

const getSpareCollars = "SELECT DISTINCT COLLAR.*, COLLAR_ALLOCATION.collar_loc_id, COLLAR_ALLOCATION.date_on, COLLAR_ALLOCATION.date_off FROM COLLAR " + 
    "LEFT JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_id = COLLAR.collar_id "; 

const getBatteryDate = "SELECT distinct BATT_DATE, BATT_MIN, BATT_MAX, BATT_00_00, BATT_04_00, BATT_08_00, BATT_12_00, BATT_16_00, BATT_20_00 " +
    "FROM BATTERY INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = BATTERY.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && batt_date = ?"

const getTempDate = "SELECT distinct TEMP_DATE,  TEMP_MAX, TEMP_00_00, TEMP_04_00, TEMP_08_00, TEMP_12_00, TEMP_16_00, TEMP_20_00 " +
    "FROM TEMPERATURE INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = TEMPERATURE.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && temp_date = ?"

const getBatteryRange = "SELECT BATT_DATE, BATT_MIN, BATT_MAX, BATT_00_00, BATT_04_00, BATT_08_00, BATT_12_00, BATT_16_00, BATT_20_00 " +
    "FROM BATTERY INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = BATTERY.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (batt_date >= ? && batt_date <= ?) ORDER BY BATTERY.BATT_DATE ASC"

const getTempRange = "SELECT TEMP_DATE, TEMP_MAX AS MAX, TEMP_00_00 AS 00_00, TEMP_04_00 AS 04_00, " +
    "TEMP_08_00 AS 08_00, TEMP_12_00 AS 12_00, TEMP_16_00 AS 16_00, TEMP_20_00 AS 20_00 " +
    "FROM TEMPERATURE INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = TEMPERATURE.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (temp_date >= ? && temp_date <= ?) ORDER BY TEMPERATURE.TEMP_DATE ASC"

const getLatestWeight = "SELECT WEIGHT.cow_id, COLLAR_ALLOCATION.collar_id, WEIGHT.weight_kg, " +
    "WEIGHT.weigh_date, WEIGHT.weighed_by FROM WEIGHT INNER JOIN COW ON COW.cow_id = WEIGHT.cow_id " +
    "INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.cow_id = WEIGHT.cow_id " +
    "INNER JOIN COLLAR ON COLLAR.collar_id = COLLAR_ALLOCATION.collar_id " +
    "WHERE COW.cow_id = ? && COLLAR_ALLOCATION.date_off IS NULL ORDER BY WEIGHT.weigh_date DESC LIMIT 1"

const getActivityDailyReport = "SELECT ACT_DATE, ACT_REST, ACT_WALK, ACT_GRAZE, ACT_BOUT " +
    "FROM activity_daily INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = activity_daily.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (act_date >= ? && act_date <= ?)"    

//Login Authentication
  

//Route for registering username and password
app.post('/register', passport.authenticate('jwt', { session: false }), async function(req, res, next) {
    try {
        let username = req.body.username;
        let pass = req.body.password;

        const password = await new Promise((resolve, reject) => {
            bcrypt.hash(pass, saltRounds, function(err, hash) {
            if (err) reject(err)
            resolve(hash, console.log(hash))
            });
        })
        
        return await User.create({ username, password })
        .then(user =>
            res.status(200).send({ user, status: '200', msg: 'Account created successfully' }))
        .catch(err => {
            if (err instanceof ValidationError) {
                res.status(400).send({status: '400', msg: "Error: Duplicate entry"})
            }
            else console.error('Server error')
        })
    } catch (error) {
        console.error("Server error");
    }  
}); 

app.post('/login', async function(req, res) {
    try {
        let username = req.body.username;
        let password = req.body.password;

        if (username && password) {
            let user = await getUser({ username });
            if (!user) {
                res.status(401).json({ msg: 'No such user found', user });
            } else {
                if (bcrypt.compareSync(password, user.password)) {
                    let payload = { id: user.id };                    
                    let token = jwt.sign(payload, jwtOptions.secretOrKey);
                    res.json({ status: '200', token: token });
                } else {
                    res.status(401).json({ msg: 'Password is incorrect' });
                } 
            }  
        }
    } catch (error) {
        console.error("Server error");
    }
});

//Critical Use Case Calls
const getActivity4secsReport = "SELECT ACT_DATE_TIME, ACT_STAT " +
    "FROM activity_four_secs_3268160 INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = activity_four_secs_3268160.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (act_date_time >= ? && act_date_time <= ?)" 

const getLocation5minsReport = "SELECT LOC_DATE_TIME, LOC_LAT, LOC_LON " +
    "FROM location_five_mins_3268160 INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = location_five_mins_3268160.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (loc_date_time >= ? && loc_date_time <= ?)" 

const getCowTemperatureReport = "SELECT TEMP_DATE, TEMP_MAX, TEMP_00_00, TEMP_04_00, TEMP_08_00,  TEMP_12_00, TEMP_16_00, TEMP_20_00 " +
    "FROM TEMPERATURE INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = TEMPERATURE.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id WHERE COW.cow_id = ? && (temp_date >= ? && temp_date <= ?)"    

const getCowWeightReport = "SELECT COW_ID, WEIGH_DATE, WEIGHED_BY, WEIGHT_KG " +
    "FROMWEIGHT WHERE cow_id = ? && (weigh_date >= ? && weigh_date <= ?)"        
   
const getUploadingFiles = "SELECT * FROM FILE_METADATA WHERE import_status = 1;"

//Critical Use Case 

//Returns 24 hourly activity counts for a given cow and day
app.get('/actday/:cowId/:date', passport.authenticate('jwt', { session: false }), (req, res) => {

    try {
        var cowId = req.params.cowId;
        var date = req.params.date;

        var isData = false;
        var promiseArray = [];
        let actAveragesSQLPromise = getAveragesSql(cowId, "act")
        actAveragesSQLPromise.then( values => {
            let actAveragesSQL = values
            for (let i = 0; i < 24; i++) {
                var j = ('0' + i).slice(-2);
                var k = ('0' + (i + 1)).slice(-2);
    
                var start = j+":00:00";
                var end = k+":00:00";
    
                promiseArray.push(new Promise(function(resolve, reject) {
                    var returndata = {};
                    dbConn.query(actAveragesSQL, [cowId, date, start, end], function(error, results) {
                        if (error) {
                            console.log(error.sqlMessage)
                            console.log("Database error, check your parameters and try again");
                            resolve();
                        } else {
                        returndata.day = date;
                        returndata.hour = (i+":00:00");
                        if (results.length > 0) {
                            isData = true;
                        }
                        returndata.hourSummaries = results;
                        resolve(returndata)
                        }
                    });                                
                }))
            }
            Promise.all(promiseArray).then(values => {
                if (isData == true) {
                    res.send(JSON.stringify({"status": 200, "error": null, "response": values}))
                } else {
                    res.send(JSON.stringify({"status": 200, "error": null, "response": null}))
                }
            }).catch(error => alert(error)); 
        })
    } catch (error) {
        console.error("Server error");
    }
});

//Returns location summaries for a given cow and day
app.get('/locday/:cowId/:date', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;
        var date = req.params.date;

        let locAveragesSQLPromise = getAveragesSql(cowId, "loc")
        locAveragesSQLPromise.then( values => {
            let locAveragesSQL = values
            dbConn.query(locAveragesSQL, [cowId, date], function(error, results) {
                if (error) {
                    console.log(error);
                    res.status(400).send("Database error, check your parameters and try again")
                } else {
                    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
                }
            });
        })
    } catch (error) {
        console.error("Server error");
    }
});

//Returns location summaries for a group of cows for a given day
app.post('/grouplocday/:date', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowGroup = req.body.cowGroup
        var date = req.params.date;
        var promiseArray = [];
        cowGroup.forEach(cowId => {
            let locAveragesSQLPromise = getAveragesSql(cowId, "loc")
                promiseArray.push(new Promise(function(resolve, reject) {
                    locAveragesSQLPromise.then( values => {
                    let locAveragesSQL = values
                    dbConn.query(locAveragesSQL, [cowId, date], function(error, results) {
                        if (error) {
                            console.log(error);
                            res.status(400).send("Database error, check your parameters and try again")
                            resolve()
                        } else {
                            resolve(results)
                        }
                    });
                });
            }));
        })
        Promise.all(promiseArray).then(values => {
            if (values) {
                res.send(JSON.stringify({"status": 200, "error": null, "response": values}))
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": null}))
            }
        }).catch(error => alert(error))
    } catch (error) {
        console.log(error)
        console.error("Server error");
    }
});

// TODO: and complete temp range
app.post('/grouplocrange/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowGroup = req.body.cowGroup
        var startDate = new Date(req.params.startDate);
        var endDate = new Date(req.params.endDate);

        var perCowPromiseArray = [];
        cowGroup.forEach(cowId => {
            let locAveragesSQLPromise = getLocRangeSql(cowId)
            perCowPromiseArray.push(new Promise(function(resolve, reject) {
                    locAveragesSQLPromise.then( values => {
                    let locAveragesSQL = values
                    dbConn.query(locAveragesSQL, [cowId, startDate, endDate], function(error, results) {
                        if (error) {
                            console.log(error);
                            res.status(400).send("Database error, check your parameters and try again")
                            resolve()
                        } else {
                            resolve(results)
                        }
                    });
                });
            }));
        })
        Promise.all(perCowPromiseArray).then(values => {
            if (values) {
                res.send(JSON.stringify({"status": 200, "error": null, "response": values}))
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": null}))
            }
        }).catch(error => alert(error))
    } catch (error) {
        console.log(error)
        console.error("Server error");
    }
});

async function getLocRangeSql(cowId) {
    var collarId = await getCollarId(cowId)
    var tableName = getTableName(collarId, "loc")
    return locationAveragesForHour = "SELECT loc_date_time, loc_lat, loc_lon FROM "+tableName+" " +
    "INNER JOIN COLLAR_ALLOCATION ON "+tableName+".collar_loc_id = COLLAR_ALLOCATION.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
    "WHERE COW.cow_id = ?  && (DATE(loc_date_time) >= ?  && DATE(loc_date_time) <= ?);"
}

//Retrieve a list of cows and their collar details
app.get('/cowlist', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        dbConn.query(returnCowList, function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(500).send("Database error, try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});

//Retrieve battery data for a cow and date
app.get('/battdate/:cowId/:date', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;
        var date = req.params.date;

        dbConn.query(getBatteryDate, [cowId, date], function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});

//Retrieve temperature for a given cow and date 
app.get('/tempdate/:cowId/:date', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;
        var date = req.params.date;
        
        dbConn.query(getTempDate, [cowId, date], function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});


// Retrieve temperature for a given group of cows and date
// Example: { "cowGroup": ["M842", "temp_79052"]}
app.post('/grouptempdate/:date', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowGroup = req.body.cowGroup;
        var date = req.params.date;
        var groupTempDate = generateGroupTempDate(cowGroup);
        
        dbConn.query(groupTempDate, date, function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.log(error)
        console.error("Server error");
    }
});


// Retrieve temperature for a given group of cows and a range
// Example: { "cowGroup": ["M842", "temp_79052"]}
app.post('/grouptempRange/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowGroup = req.body.cowGroup;
        var startDate = req.params.startDate;
        var endDate = req.params.endDate;

        var groupTempRange = generateGroupTempRange(cowGroup);
        console.log("GroupTempRange:");
        console.log(cowGroup);
        console.log("StartDate: " + startDate + " End Date: " + endDate)
        
        dbConn.query(groupTempRange, [startDate, endDate], function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.log(error)
        console.error("Server error");
    }
});

function generateGroupTempRange(cowGroup) {
    let baseSQL = "SELECT TEMP_DATE, AVG(TEMP_MAX) AS MAX, ((AVG(TEMP_00_00) + AVG(TEMP_04_00) + AVG(TEMP_08_00) + AVG(TEMP_12_00) + AVG(TEMP_16_00) + AVG(TEMP_20_00))/(" + cowGroup.length + ")) AS AVG, " +
    "LEAST(AVG(TEMP_00_00), AVG(TEMP_04_00), AVG(TEMP_08_00), AVG(TEMP_12_00), AVG(TEMP_16_00), AVG(TEMP_20_00)) AS MIN " +
    "FROM TEMPERATURE INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = TEMPERATURE.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
    "WHERE (temp_date >= ? && temp_date <= ?) && ("
    cowGroup.forEach( (element, index) => {
        if((index + 1) === cowGroup.length){
            baseSQL += ` COW.cow_id = "${element}")`
        } else {
            baseSQL += ` COW.cow_id = "${element}" || `
        }
    })
    baseSQL += " GROUP BY TEMPERATURE.TEMP_DATE"
    return baseSQL
}

function generateGroupTempDate(cowGroup) {
    let baseSQL = "SELECT TEMP_DATE, AVG(TEMP_MAX) AS MAX, AVG(TEMP_00_00) AS 00_00, AVG(TEMP_04_00) AS 04_00, AVG(TEMP_08_00) AS 08_00, AVG(TEMP_12_00) AS 12_00, AVG(TEMP_16_00) AS 16_00, AVG(TEMP_20_00) AS 20_00 " +
    "FROM TEMPERATURE INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = TEMPERATURE.collar_loc_id " +
    "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " +
    "WHERE temp_date = ? && ("
    cowGroup.forEach( (element, index) => {
        if((index + 1) === cowGroup.length){
            baseSQL += ` COW.cow_id = "${element}")`
        } else {
            baseSQL += ` COW.cow_id = "${element}" || `
        }
    })
    baseSQL += " GROUP BY TEMPERATURE.TEMP_DATE"
    return baseSQL
}

//Retrieve battery readings for a date range
app.get('/battrange/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate;
        var endDate = req.params.endDate;

        dbConn.query(getBatteryRange, [cowId, startDate, endDate], function (error, results, field) {
            if (error){
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                var arr = createBattRangeArray(results);
                res.send({"status": 200, "error": null, "response": arr});
            }
        });
    } catch {
        console.error("Server error");
    }
});


//Retrieve temperature readings for a date range
app.get('/temprange/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate;
        var endDate = req.params.endDate;

        dbConn.query(getTempRange, [cowId, startDate, endDate], function (error, results, field) {
            if (error){
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                var arr = createTempRangeArray(results);
                res.send({"status": 200, "error": null, "response": arr});
            }
        });
    } catch {
        console.error("Server error");
    }
});

//Retrieve a list of cows and their entry details
app.get('/cow', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        dbConn.query(returnCows, function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(500).send("Database error, try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});

//Get details for a specific cow
app.get('/cow/:cow_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cow_id;
        let sql = 'SELECT * FROM COW WHERE cow_id = ?';
        dbConn.query(sql, cowId, function (error, results, field) {
            if(error) throw error;
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    } catch {
        console.error("Server error");
    }
});

//Add a new cow
app.post('/cow', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        let today = getCurrentDate();
        let cowAdd = {cow_id: req.body.cow_id, date_of_birth: req.body.date_of_birth, date_of_entry: today};
        let sql = "INSERT INTO COW SET ? ";    
        dbConn.query(sql, cowAdd, (error, results) => {
            if (error) {
                if (error.code === 'ER_DUP_ENTRY') {
                    res.status(400).send("Error: Duplicate entry")
                } 
            } else {
                res.status(201).send(JSON.stringify({"status": 201, "error": null, "response": results}))
            }
        });
    } catch {
        console.error("Server error")
    }
});

//Update details for a cow
app.put('/cow/:cow_id', passport.authenticate('jwt', { session: false }), (req,res) => {
    try {
        var cowId = req.params.cow_id;
        var dob = checkValidDate(req.body.date_of_birth);
        var doe = checkValidDate(req.body.date_of_entry);
        var doex = checkValidDate(req.body.date_of_exit);

        dbConn.query(cowPut, [cowId, dob, doe, doex, cowId], function (error, results) {
            if(error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch {
        console.log(cowPut + [cowId, dob, doe, doex, cowId])
        console.error("Server error")
    }
});

//Retire a cow
app.patch('/cow/:cow_id', passport.authenticate('jwt', { session: false }), (req,res) => {
    try{
        var cowId = req.params.cow_id;
        var doe = getCurrentDate();

        var sql = "UPDATE COW SET date_of_exit = ? WHERE cow_id = ?"
        dbConn.query(sql, [doe, cowId], function (error, results) {
            if(error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                console.log(sql, doe, cowId);
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });        
    } catch {
        console.error("Server error")
    }
})

//Delete a cow entry
app.delete('/cow/:cow_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        let sql = "DELETE FROM COW WHERE cow_id = ?"
        let cowId = req.params.cow_id;
        dbConn.query(sql, cowId, function (error, results) {
            if(error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch {
        console.error("Server error")
    }
});

//Retrieve the latest cow weight, collar and cow details from cow id
app.get('/weight/:cowId', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        var cowId = req.params.cowId;

        dbConn.query(getLatestWeight, [cowId], function (error, results, field) {
            if (error){
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch {
        console.error("Server error");
    }
});


// Add collar / update collar use case calls 

//Retrieve all collars -(postman call: GET http://localhost:3000/collar/)
app.get('/collar', passport.authenticate('jwt', { session: false }), (req, res) => {
    dbConn.query(returnCollars, function (error, results) {
        if(error) {
            console.log(error)
            res.status(500).send("Error getting list of collars")
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        }
    });
});

//Retrieve a single collar data entry by collar id -(postman call: GET http://localhost:3000/collar/1234/)
app.get('/collar/:collar_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    dbConn.query('SELECT * FROM collar WHERE collar_id='+req.params.collar_id, function (error, results, field) {
        if(error) {
            console.log(error)
            res.status(500).send("Error getting collar: " + res + " details")
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        }
    });
    ;
})

//Add a new collar -(postman call: POST http://localhost:3000/collar/) 
// - body JSON { "collar_id": "1234","collar_ser_no": "4324223235", "collar_num": "s19"}
app.post('/collar', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        let today = getCurrentDate();
        let collAdd = {collar_id: req.body.collar_id, collar_ser_no: req.body.collar_ser_no, collar_num: req.body.collar_num, date_of_entry: today};
        let sql = "INSERT INTO COLLAR SET ? ";
        console.log("Creating collar:")
        console.log(collAdd)
        dbConn.query(sql, collAdd, (error, results) => {
            if(error) {
                console.log(error);
                res.status(500).send("Error creating collar: " + collAdd)
            } else {
                res.status(201).send(JSON.stringify({"status": 201, "error": null, "response": results}));
            }
        });
    } catch {
        console.error("Server error")
    }
});

//Update details for a collar with collar id - (postman call: PUT http://localhost:3000/collar/5678/) 
// - body JSON { "collar_ser_no": "555555555","collar_num": "s25", "date_of_entry": "2020-03-05"}
app.put('/collar/:collar_id', passport.authenticate('jwt', { session: false }), (req,res) => {
    try {
        var collId = req.params.collar_id;
        var collSer = req.body.collar_ser_no;
        var collNum = req.body.collar_num;

        if (!collId || !collSer || !collNum) {
            return res.status(400).send({ error: cow, message: 'Missing a field for update' });
        }

        var sql = "UPDATE COLLAR SET collar_id = ?, collar_ser_no = ?, collar_num = ? WHERE collar_id = ?";
        dbConn.query(sql, [collId, collSer, collNum, collId], function (error, results, fields) {
            if (error) {
                console.log(error);
                res.status(500).send("Error updating collar: " + collId)
            }
           res.status(201)
           res.send(JSON.stringify({"status": 201, "error": null, "response": results})); 
        });
    } catch (err) {
        console.log(err)
    }
});

//Delete a collar by collar id -(postman call: DELETE http://localhost:3000/collar/5678)
app.delete('/collar/:collar_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    try{
        let sql = "DELETE FROM COLLAR WHERE collar_id = ?"
        let collarId = req.params.collar_id;
        dbConn.query(sql, collarId, function (err, results) {
            if(err) {
                console.log(err);
                res.status(500);
                if(err.sqlMessage.includes("foreign key constraint fails")){
                    res.send(JSON.stringify({"status": 500, "error": "Collar allocation exists", "response": null}));
                } else {
                    res.send(JSON.stringify({"status": 500, "error": "Error deleting collar ", "response": null}));
                }
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch {
        console.log("Server error")
    }
  });


// Add weight / update weight use case calls  

//Retrieve all weight data -(postman call: GET http://localhost:3000/weight/)
app.get('/weight', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        dbConn.query('SELECT * FROM WEIGHT', function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(500).send("Database error, try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});


//Add a new weight entry -(postman call: POST http://localhost:3000/weight/) 
// - body JSON { "cow_id": "M674","weigh_date": "2020-03-04", "weighed_by": "Fred", "weight_kg": "657"}
app.post('/weight', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
       let weightMeasure = {cow_id: req.body.cow_id, weigh_date: req.body.weigh_date,
       weighed_by: req.body.weighed_by, weight_kg: req.body.weight_kg};
       let sql = "INSERT INTO WEIGHT SET ? "; 
       dbConn.query(sql, weightMeasure, (error, results) => {
        if(error) {
            console.log(error);
            res.status(500).send("Database error, try again")
        } else {
            res.status(201).send(JSON.stringify({"status": 201, "error": null, "response": results}));
        }
    });
    } catch {
        console.error("Server error")
    }
});


//Delete a weight by weight_id -(postman call: DELETE http://localhost:3000/weight/1)
app.delete('/weight/:weight_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    try{
        let sql = "DELETE FROM WEIGHT WHERE weight_id="+req.params.weight_id+"";
        dbConn.query(sql, (err, results) => {
        if(err) {
            console.log(err);
            res.status(500);
            res.send(JSON.stringify({"status": 500, "error": "Error deleting collar ", "response": null}));
        } else{ 
            res.send(JSON.stringify({"status": 200, "error": null, "response": results})); 
        }
        });
    } catch {
        console.error("Server error")
    }
});  

//Returns a list of unallocated collars
  app.get('/allocate', passport.authenticate('jwt', { session: false }), (req, res) => {
      try {
        dbConn.query(getSpareCollars, function (error, results, field) {
            if (error) {
                console.log(error);
                res.status(500).send("Database error, try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});

//Allocate a collar to a cow
app.post('/allocate', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        let today = getCurrentDate();
        let cow_id = req.body.cow_id;
        let collar_id = req.body.collar_id;

        console.log(allocateCollar)
        console.log(cow_id, collar_id, today)
        dbConn.query(allocateCollar, [cow_id, collar_id, today], (error, results) => {
            if (error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.status(201).send(JSON.stringify({"status": 201, "error": null, "response": results}))
            }
        });
    } catch {
        console.error("Server error")
    }
});

//Updates collar allocation date on the removal of a collar
app.patch('/allocate/:collar_id', passport.authenticate('jwt', { session: false }), (req,res) => {
    try{
        var collId = req.params.collar_id;
        var doe = getCurrentDate();
        console.log(removeCollar, doe, collId);

        dbConn.query(removeCollar, [doe, collId], function (error, results) {
            if(error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });        
    } catch {
        console.error("Server error")
    }
})

//Gets collar id based on cow id
app.get('/getcollar/:cowId', passport.authenticate('jwt', { session: false }), (req,res) => {
    try{
        var cowId = req.params.cowId;

        let sql = 'SELECT collar_id FROM COLLAR_ALLOCATION WHERE cow_id = ? ORDER BY collar_loc_id DESC LIMIT 1'
        dbConn.query(sql, cowId, function (error, results) {
            if(error) {
                console.log(error);
                res.status(400).send("Database error, check your parameters and try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            }
        });        
    } catch {
        console.error("Server error")
    }
})

//Update a cow weight measurement by weight id (postman call: PUT http://localhost:3000/weight/3) 
// - body JSON { "cow_id": "M777","weigh_date": "1900-03-05", "weighed_by": "Harry","weight_kg": "33333"}
app.put('/weight/:weight_id', passport.authenticate('jwt', { session: false }), (req,res) => {
    try{
        var cowId = req.body.cow_id;
        var wDate = req.body.weigh_date;
        var wBy = req.body.weighed_by;
        var wKG = req.body.weight_kg;

        if (!cowId || !wDate || !wBy || !wKG) {
            return res.status(400).send({ error: "weight", message: 'Missing a field for update' });
        }
        dbConn.query("UPDATE WEIGHT SET cow_id = ?, weigh_date = ?, weighed_by = ?, weight_kg = ? WHERE weight_id ="+req.params.weight_id, [cowId, wDate, wBy, wKG], function (error, results) {
            if (error) {
                console.log(error);
                res.status(500).send("Error updating weight: ")
            } 
            res.status(200)
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            });
        } catch (error) {
            console.log(error)
        }
});  

//Retrieves a list of all cow id's (postman call: GET http://localhost:3000/cowId)
app.get('/cowId', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
        dbConn.query(returnAllCowID, function (error, results) {
            if (error) {
                console.log(error);
                res.status(500).send("Database error, try again")
            } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results})); 
            }
        });
    } catch (error) {
        console.error("Server error");
    }
});


// activity daily report
app.get('/activityDailyReport/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    const tempFile = fs.createWriteStream(tempDailyFileName,{ flags: 'w' });
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate;
        var endDate = req.params.endDate;
        dbConn.query(getActivityDailyReport, [cowId, startDate, endDate], function (error, results) {
                if(isEmpty(results)) {
                    console.log("HERE" + results)
                    res.send(JSON.stringify({"status": 500, "error": "Out of range query, check your parameters and try again ", "response": null}));
                }

                else if (error){
                    console.log("asasas" + error);
                    res.status(400).send("Database error, check your parameters and try again")

                } else if(!isEmpty(results)){
                    res.send(JSON.stringify({"status": 200, "message": "Generating report ", "response": null}));
                    const jsonData = JSON.parse(JSON.stringify(results))
                    console.log("jsonData", jsonData);
                    fastcsv
                        .write(jsonData, { headers: true })
                        .on("finish", function() {
                            console.log("Written!");
                    })
                    .pipe(tempFile);
                }
        });
    } catch (error) {
        console.error("Server error");
    }
});


function getCollarIdDate(cowId, startDate, endDate){  
    return new Promise(async function(resolve, reject) {
        try {
            let sql = 'SELECT * FROM COLLAR_ALLOCATION WHERE COW_ID = ?;'
            await dbConn.query(sql, [ cowId, startDate, endDate], function (error, results, field) {

            //let sql = 'SELECT COLLAR_ID FROM smartfarm.COLLAR_ALLOCATION WHERE (COW_ID = ? && DATE_ON <= ? );'
            //let results = await dbConn.query(sql, [cowId, startDate], function (error, results) {     
                if (error) {
                    console.log(error);
                    reject(error)
                }
                else {
                    console.log(results)
                    resolve(results);
                }
            });
        } catch (error){
            console.error("Server error");
            reject()
        }
    })
}

async function getCsvReport(cowId, startDate, endDate, tableType) {    
    var collarIdList = await getCollarIdDate(cowId, startDate, endDate)

    let collarQueries = []
    collarIdList.forEach((collar) => {
        var tableName = getTableName(collar.COLLAR_ID, tableType)
        if (tableType == "act"){
            activity4seconds = "SELECT ACT_DATE_TIME, ACT_STAT " +
                "FROM " + tableName +
                " INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = " + tableName + " .collar_loc_id " +
                "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " + 
                "WHERE COW.cow_id = ? && (act_date_time >= ? && act_date_time <= ?)"
            collarQueries.push(activity4seconds) 
        } else if (tableType === "loc") {
            location5mins = "SELECT LOC_DATE_TIME, LOC_LAT, LOC_LON " +
                "FROM " + tableName + 
                " INNER JOIN COLLAR_ALLOCATION ON COLLAR_ALLOCATION.collar_loc_id = " + tableName + " .collar_loc_id " +
                "INNER JOIN COW ON COLLAR_ALLOCATION.cow_id = COW.cow_id " + 
                "WHERE COW.cow_id = ? && (loc_date_time >= ? && loc_date_time <= ?)"
            collarQueries.push(location5mins)
        }
    })
    return collarQueries
}

// activity 4 secs Report
app.get('/activity4secsReport/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    const tempFile = fs.createWriteStream(tempAct4FileName,{ flags: 'w' }); 
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate + " 00:00:00";
        var endDate = req.params.endDate + " 23:59:59";

        let activity4secsPromise = getCsvReport(cowId, startDate, endDate, "act")
        activity4secsPromise.then( async queries => {
            let errors = []
            let combinedResults = []
            await new Promise( async (resolve, reject) => {
                await queries.forEach( async (query, index, array) => {
                    await dbConn.query(query, [cowId, startDate, endDate], await async function (error, results) {
                        if(error){
                            if (!error.message.includes("ER_NO_SUCH_TABLE")){
                                console.log(error.message)
                                errors.push(error)
                            }
                        }
                        if(results){
                            combinedResults = combinedResults.concat(results)
                        }
                        if (index === array.length -1) resolve();
                    })
                })
            }).then( () => {
                if(errors.length > 0){
                    console.log(errors)
                    res.status(400).send("Database error, check your parameters and try again")
                } else {
                    res.send(JSON.stringify({"status": 200, "message": "Generating report ", "response": null}));
                    const jsonData = JSON.parse(JSON.stringify(combinedResults))
                    fastcsv.write(jsonData, { headers: true })
                        .on("finish", function() {
                            console.log("Written!");
                        })
                        .pipe(tempFile);
                }
            })
        })
    } catch (error) {
        console.error("Server error");
    }
});

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// location 5 mins reports
app.get('/location5minsReport/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    const tempFile = fs.createWriteStream(tempLocationFiveFileName,{ flags: 'w' });
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate + " 00:00:00";
        var endDate = req.params.endDate + " 23:59:59";

        let location5minPromise = getCsvReport(cowId, startDate, endDate, "loc")
        location5minPromise.then( async queries => {
            let errors = []
            let combinedResults = []
            await new Promise( async (resolve, reject) => {
                await queries.forEach( async (query, index, array) => {
                    await dbConn.query(query, [cowId, startDate, endDate], await async function (error, results) {
                        if(error){
                            if (!error.message.includes("ER_NO_SUCH_TABLE")){
                                console.log(error.message)
                                errors.push(error)
                            }
                        }
                        if(results){
                            combinedResults = combinedResults.concat(results)
                        }
                        if (index === array.length -1) resolve();
                    })
                })
            }).then( () => {
                if(errors.length > 0){
                    console.log(errors)
                    res.status(400).send("Database error, check your parameters and try again")
                } else {
                    res.send(JSON.stringify({"status": 200, "message": "Generating report ", "response": null}));
                    const jsonData = JSON.parse(JSON.stringify(combinedResults))
                    fastcsv.write(jsonData, { headers: true })
                        .on("finish", function() {
                            console.log("Written!");
                        })
                        .pipe(tempFile);
                }
            })
        })
    } catch (error) {
        console.error("Server error");
    }
});

// cow temperature report
app.get('/cowTemperatureReport/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    const tempFile = fs.createWriteStream(tempTempFileName,{ flags: 'w' });
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate + " 00:00:00";
        var endDate = req.params.endDate + " 23:59:59";

        dbConn.query(getCowTemperatureReport, [cowId, startDate, endDate], function (error, results) {            
            if(isEmpty(results)) {
                console.log("HERE" + results)
                res.send(JSON.stringify({"status": 500, "error": "Out of range query, check your parameters and try again ", "response": null}));
            }
            else if (error){
                console.log("asasas" + error);
                res.status(400).send("Database error, check your parameters and try again")
           
            } else if(!isEmpty(results)){
                res.send(JSON.stringify({"status": 200, "message": "Generating report ", "response": null}));
                const jsonData = JSON.parse(JSON.stringify(results))
                console.log("jsonData", jsonData);
                fastcsv
                    .write(jsonData, { headers: true })
                    .on("finish", function() {
                        console.log("Written!");
                    })
                    .pipe(tempFile);
                }
        });
    
    } catch (error) {
        console.error("Server error");
    }
});

//cow weight report
app.get('/cowWeightReport/:cowId/:startDate/:endDate', passport.authenticate('jwt', { session: false }), (req, res) => {
    const tempFile = fs.createWriteStream(tempWeightFileName,{ flags: 'w' });
    try {
        var cowId = req.params.cowId;
        var startDate = req.params.startDate;
        var endDate = req.params.endDate;

        dbConn.query(getCowWeightReport, [cowId, startDate, endDate], function (error, results) {            
            if(isEmpty(results)) {
                console.log("HERE" + results)
                res.send(JSON.stringify({"status": 500, "error": "Out of range query, check your parameters and try again ", "response": null}));
            }
            else if (error){
                console.log("asasas" + error);
                res.status(400).send("Database error, check your parameters and try again")
            
            } else if(!isEmpty(results)){
                res.send(JSON.stringify({"status": 200, "message": "Generating report ", "response": null}));
                const jsonData = JSON.parse(JSON.stringify(results))
                console.log("jsonData", jsonData);
                fastcsv
                    .write(jsonData, { headers: true })
                    .on("finish", function() {
                        console.log("Written!");
                    })
                    .pipe(tempFile);
                }
        });
    
    } catch (error) {
        console.error("Server error");
    }
});

app.use(express.static('public')); /* this line tells Express to use the public folder as our static folder from which we can serve static files*/
var path = require("path");

app.get('/ActDailyReport', function(req, res){
    res.sendFile(path.join(__dirname + '/' + tempDailyFileName));   
});

app.get('/ActFourSecondsReport', function(req, res){
    res.sendFile(path.join(__dirname + '/' + tempAct4FileName));   
});

app.get('/LocationFiveReport', function(req, res){
    res.sendFile(path.join(__dirname + '/' + tempLocationFiveFileName));   
});

app.get('/TempReport', function(req, res){
    res.sendFile(path.join(__dirname + '/' + tempTempFileName));   
});

app.get('/WeightReport', function(req, res){
    res.sendFile(path.join(__dirname + '/' + tempWeightFileName));   
});



// File import status
app.get('/uploadingFiles', passport.authenticate('jwt', { session: false }), (req, res) => {
    try {
      dbConn.query(getUploadingFiles, function (error, results, field) {
          if (error) {
              console.log(error);
              res.status(500).send("Database error, try again")
          } else {
              res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
          }
      });
  } catch (error) {
      console.error("Server error");
  }
});


module.exports = server.listen(port, () => {
    console.log('App is running on port ' + port);
});
