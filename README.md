# README #


### What is this repository for? ###

* The SQL folder contains the SQL script - init.sql to set up the smartfarm database
* The smartFarmDB-API folder contains a nodeJS application interface for the smartfarm MySQL database

### How do I get set up? ###

* Run git `submodule init` and `git submodule update` to pull down the dataimport submodule
* Open the SQL file init.sql
* Open MySQL WorkBench
* Copy and paste code directly into a MySQL query, or save then open file via MySQL
* Run MySQL - database is created

* Install nodeJS
* Type 'npm install' in the backend/smartFarmDB-API directory.
* Type 'node index' in the backend/smartFarmDB-API directory.

* Integration and Unit tests are contained in backend/smartFarmDM-API/test
* To run Mocha tests type 'npm test' in the backend/smartFarmDB-API directory.
* Extra integration tests for Postman are found in the test folder
* To run Postman tests, install Postman from https://www.postman.com/
* In Postman select File > Import > Upload Files and select Integration Tests.postman_collection.json
* Select Runner > Integration Tests to run Postman Collection
* A copy of local Postman test runs is saved in tests/Integration Tests.postman_test_run.json
* NB - testing depends on local data. Change const cow to whichever local cow_id is available. Weight data must also be added.




